<%@ page import="java.io.File" %>
<%@ page import="java.util.List" %>
<%@ page import="ex07.ImageGalleryDisplay" %>
  Created by IntelliJ IDEA.
  User: mshe666
  Date: 17/01/2018
  Time: 2:41 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Title</title>
</head>
<body>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<table>
    <tr><th>Thumbnail</th>
        <th><a href='/ImageGalleryDisplay?sortColumn=filename&order=${filenamesortoggle}ending'>Filename <img src='/images/sort-${currnamesorttoggle}.png' alt='icon' /></a></th>
        <th><a href='/ImageGalleryDisplay?sortColumn=filesize&order=${filesizesorttoggle}ending'>File-size <img src='/images/sort-${currfilesizesorttoggle}.png'  alt='icon' /></a></th>
    </tr>



    <c:forEach var="fileObjects" items="${fileObjects}">
        <tr>
            <td>
                <a href="Photos/${fileObjects.filename}">
                    <img src="Photos/${fileObjects.getThumbRelative()}"
                         alt="${fileObjects.thumbDisplay}">
                </a>
            </td>
            <td>${fileObjects.thumbDisplay}</td>
            <td>${fileObjects.fullfileSize}</td>
        </tr>
    </c:forEach>
</table>

</body>
</html>
